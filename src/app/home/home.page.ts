import { Component } from '@angular/core';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  tasks : any[] = [];
  constructor(private alertCtrl : AlertController, private toastCtrl : ToastController, private actionSheetCtrl : ActionSheetController ) {
    let taskJson = localStorage.getItem('taskDb');

    if (taskJson!=null){
      this.tasks = JSON.parse(taskJson)
    }
  }

  async   ShowAdd() {
    const alert = await this.alertCtrl.create({
      header: "O que deseja fazer?",
      inputs: [
        {
          name: 'task',
          type: 'text',
          placeholder: 'Informe a tarefa'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('clicked cancel')
          }
        },
        {
          text: 'Adicionar',
          handler: (form) => {
            this.add(form.task);
          }
        }
      ]
    });

    await alert.present();
  }

  async add(newTask : string) {
    //validate if newTask is not empty
    if (newTask.trim().length < 1) {
      const toast = await this.toastCtrl.create({
        message: 'Você não informou sua tarefa!',
        duration: 2000,
        position: 'bottom',
        color: 'danger'
      });

      toast.present();
      return;
    }

    let task = {name : newTask, done: false};
    this.tasks.push(task);

    this.updateLocalStorage();
  }

  updateLocalStorage(){
    localStorage.setItem('taskDb', JSON.stringify(this.tasks));
  }

  async OpenActions(task : any){
    const actionSheet = await this.actionSheetCtrl.create({
      header: 'O QUE DESEJA FAZER?',
      buttons: [
        {
          text: task.done ? 'Desmarcar' : 'Marcar',
          icon: task.done ? 'radio-button-off' : 'checkmark-circle',
          handler: ()=> {
            task.done = !task.done;

            this.updateLocalStorage();
          }
        },
        {
          text: 'Deletar Tarefa',
          icon: 'trash-outline',
          handler: () => {
            this.delete(task);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('cancelled');
          }
        }
    ]
    });
    await actionSheet.present();
  }


  delete(task : any) {
    this.tasks = this.tasks.filter(taskArray=> task != taskArray);

    this.updateLocalStorage();
  }

}
